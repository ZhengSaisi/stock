package stock

import (
	"fmt"
	"log"
	"os"
	"sync"

	"gorm.io/gorm"
)

type monitor struct {
	wg            sync.WaitGroup
	s             *Stock
	ctrlSell      chan controlSell
	ctrlBuy       chan controlBuy
	ctrlFetchRecv chan controlFetchRecv
	ctrlFetchSend chan controlFetchSend
}

type controlSell struct {
	isSellTime bool
	msg        string
	err        error
}

type controlBuy struct {
	isBuyTime bool
	msg       string
	err       error
}

type controlFetchRecv struct {
	code string
}

type controlFetchSend struct {
	code string
	stockDataColl
	ok  bool
	err error
}

func ServeMonitorStocks() error {
	var mon monitor
	var db *gorm.DB
	var pTradeDB *TradeDB
	var err error
	db, err = NewDB()
	if err != nil {
		log.Printf("can't connect to database, err=%v\n", err)
		return err
	}

	pTradeDB = (*TradeDB)(db)
	mon.s = Default()

	mon.s.Fetch()

	codeColl := mon.s.GetCodes()
	mon.s.importDB(pTradeDB)
	log.Println("import db success!!!")

	mon.ctrlBuy = make(chan controlBuy, len(codeColl))

	go mon.monitorBuy(codeColl, pTradeDB)

	mon.controllerBuy()
	return nil
}

func (mon *monitor) controllerBuy() {
	os.Remove("monitorResult")
	file, err := os.OpenFile("monitorResult", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatalln("can't open codeColl.txt")
	}
	defer file.Close()
	for res := range mon.ctrlBuy {
		if res.err != nil {
			log.Printf("err=%v\n", res.err)
		} else {
			if res.isBuyTime {
				file.WriteString(res.msg)
			}
		}
	}
}

func (mon *monitor) monitorBuy(coll codeColl, pTradeDB *TradeDB) error {
	var l *limit = newLimit(1)

	for _, c := range coll {
		if c != "" {
			mon.wg.Add(1)
			f := func() {
				mon.monitorBuyCode(c, pTradeDB)
			}
			l.Run(f)
		}
	}

	mon.wg.Wait()

	close(mon.ctrlBuy)
	return nil
}

func (mon *monitor) monitorBuyCode(code string, pTradeDB *TradeDB) {
	defer mon.wg.Done()
	ind, err := pTradeDB.genIndicator(code)
	if err != nil {
		log.Printf("generate Indicator of stock failed, err=%v\n", err)
		mon.ctrlBuy <- controlBuy{false, "", err}
		return
	}
	isBuyTime := ind.isBuyTime()
	if isBuyTime {
		msg := fmt.Sprintf("Buy Time\ncode = %s\n\n", code)
		mon.ctrlBuy <- controlBuy{isBuyTime, msg, nil}
	}
}

module stock

go 1.17

require (
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.2
)

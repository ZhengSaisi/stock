package stock

type limit struct {
	n  int
	ch chan struct{}
}

func newLimit(n int) *limit {
	return &limit{
		n:  n,
		ch: make(chan struct{}, n),
	}
}

func (l *limit) Run(f func()) {
	l.ch <- struct{}{}
	go func() {
		f()
		<-l.ch
	}()
}

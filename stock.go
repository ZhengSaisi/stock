package stock

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

const codeCollAPI = "http://www.shdjt.com/js/lib/astock.js"

//https://web.ifzq.gtimg.cn/appstock/app/fqkline/get?param=sh600519,day,2020-3-1,2021-3-1,500,
const historyReqAPI = "https://web.ifzq.gtimg.cn/appstock/app/fqkline/get?param=%s,day,,%s,%d,"

const realTimeReqAPI = "https://qt.gtimg.cn/q=%s"

const MinFetchIntervalTraDay = 360 * 3
const filename = "codeColl.txt"

type codeColl []string
type Stock struct {
	codeColl
	req
	respColl

	sidewaysPercent int
	policy          int
	onlyASHstock    bool
}

type req struct {
	start          string
	end            string
	intervalTraDay int
}

type respColl struct {
	wg    sync.WaitGroup
	mutex sync.Mutex
	resps []resp
}

type resp struct {
	code           string
	name           string
	PETTM          float64
	PEDYNAMIC      float64
	ChangeHandRate float64
	stockDataColl
}

type stockDataColl []stockData

type historyMsg struct {
	Code int                    `json:"code"`
	Msg  string                 `json:"msg"`
	Data map[string]interface{} `json:"data"`
}

type stockData struct {
	code       string
	traDay     string
	startPrice float64
	endPrice   float64
	highPrice  float64
	lowPrice   float64
	totalHand  float64
}

func Default() *Stock {
	var stock Stock

	stock.req.start = ""
	stock.req.end = time.Now().Format("2006-01-02")
	stock.req.intervalTraDay = 120
	stock.sidewaysPercent = 50
	stock.policy = 1
	stock.onlyASHstock = true

	return &stock
}

func (pStock *Stock) SetIntervalTraDay(interval int) {
	pStock.req.intervalTraDay = interval
}

func (pStock *Stock) SetSidewaysPercent(percent int) {
	pStock.sidewaysPercent = percent
}

func (pStock *Stock) SetPricePolicy(policy int) {
	pStock.policy = policy
}

func (pStock *Stock) SetonlyASHstock(onlyASHstock bool) {
	pStock.onlyASHstock = onlyASHstock
}

func (pStock *Stock) SetCodeColl(codeColl []string) {
	pStock.codeColl = codeColl
}

func (pStock *Stock) UpdateCodeColl() {
	resp, err := http.Get(codeCollAPI)
	if err != nil {
		log.Fatalln("no response in http GET")
	}
	defer resp.Body.Close()
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fatalln("can't open codeColl.txt")
	}
	defer file.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("can't read http Body")
	}
	defer resp.Body.Close()
	file.Write(content)
}

func removeDuplicateElement(elements []string) []string {
	result := make([]string, 0, len(elements))
	temp := map[string]struct{}{}
	for _, item := range elements {
		if _, ok := temp[item]; !ok {
			temp[item] = struct{}{}
			result = append(result, item)
		}
	}
	return result
}

func isDuplicateElement(elements []string, element string) bool {
	for _, item := range elements {
		if item == element {
			return true
		}
	}
	return false
}

func (pStock *Stock) ReadCodeColl() []string {
	var result []string
	var prefix []string = []string{"sh", "sz", "zz"}
	var shHead []string = []string{"50", "51", "60", "90", "110", "113", "118", "132", "204", "5", "6", "9", "7"}
	var aHead []string = []string{"60", "00"}
	var content []byte

	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("can't open %s\n", filename)
	}
	content, err = ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("can't get content from %s\n", filename)
	}
	r, err := regexp.Compile("~([a-z0-9]*)`")
	if err != nil {
		log.Fatalln("can't compile regex")
	}
	all := r.FindAllStringSubmatch(string(content), -1)
	result = make([]string, 0, len(all))

	for _, j := range all {
		var val string
		if len(j) > 1 {
			val = j[1]
			isHasShHead := false
			for _, pref := range prefix {
				val = strings.TrimPrefix(val, pref)
			}
			if pStock.onlyASHstock {
				isHasAHead := false
				for _, a := range aHead {
					if strings.HasPrefix(val, a) {
						isHasAHead = true
						break
					}
				}
				if !isHasAHead {
					continue
				}
			}
			for _, head := range shHead {
				if strings.HasPrefix(val, head) {
					val = fmt.Sprintf("sh%s", val)
					isHasShHead = true
					break
				}
			}
			if !isHasShHead {
				val = fmt.Sprintf("sz%s", val)
			}
			result = append(result, val)
		}
	}
	result = removeDuplicateElement(result)
	return result
}
func parseHistoryContent(res *resp, content []byte, code string) error {
	var msg historyMsg
	var stockDataColl stockDataColl = make(stockDataColl, 0)
	var err error

	err = json.Unmarshal(content, &msg)
	if err != nil {
		log.Println("can't parse json")
		return err
	}

	for k, v := range msg.Data {
		if strings.Compare(k, code) == 0 {
			v2, ok := v.(map[string]interface{})
			if ok {
				for k, v := range v2 {
					if strings.Compare(k, "day") == 0 {
						v3, ok := v.([]interface{})
						if ok {
							for _, v := range v3 {
								var stockData stockData
								v4, ok := v.([]interface{})
								if ok {
									for k, v := range v4 {
										v5, ok := v.(string)
										if ok {
											switch k {
											case 0:
												stockData.traDay = v5
											case 1:
												startPrice, err := strconv.ParseFloat(v5, 64)
												if err != nil {
													log.Fatalln("parse string to float fail")
												}
												stockData.startPrice = startPrice
											case 2:
												endPrice, err := strconv.ParseFloat(v5, 64)
												if err != nil {
													log.Fatalln("parse string to float fail")
												}
												stockData.endPrice = endPrice
											case 3:
												highPrice, err := strconv.ParseFloat(v5, 64)
												if err != nil {
													log.Fatalln("parse string to float fail")
												}
												stockData.highPrice = highPrice
											case 4:
												lowPrice, err := strconv.ParseFloat(v5, 64)
												if err != nil {
													log.Fatalln("parse string to float fail")
												}
												stockData.lowPrice = lowPrice
											case 5:
												totalHand, err := strconv.ParseFloat(v5, 64)
												if err != nil {
													log.Fatalln("parse string to float fail")
												}
												stockData.totalHand = totalHand
											default:
												log.Fatalln("error key")
											}
										}
									}
								}
								stockData.code = code
								stockDataColl = append(stockDataColl, stockData)
							}
						}
					}
				}
			}
		}
	}
	res.code = code
	res.stockDataColl = stockDataColl
	return nil
}

func parseRealTimeContent(res *resp, content []byte) error {
	var msgSplit []string
	msgSplit = strings.Split(string(content), "~")
	var err error

	if len(msgSplit) > 39 {
		if msgSplit[38] != "" {
			res.ChangeHandRate, err = strconv.ParseFloat(msgSplit[38], 64)
			if err != nil {
				return err
			}
		} else {
			res.ChangeHandRate = -1
		}
		if msgSplit[39] != "" {
			res.PETTM, err = strconv.ParseFloat(msgSplit[39], 64)
			if err != nil {
				return err
			}
		} else {
			res.PETTM = -1
		}
		if msgSplit[52] != "" {
			res.PEDYNAMIC, err = strconv.ParseFloat(msgSplit[52], 64)
			if err != nil {
				return err
			}
		} else {
			res.PEDYNAMIC = -1
		}
	}

	return nil
}

func (pStock *Stock) Filter(res *resp, names map[string]string) (bool, error) {
	var isNeedFilter bool = true
	if res != nil && len(res.stockDataColl) >= MinFetchIntervalTraDay {
		dataColl := res.stockDataColl
		sort.Sort(sort.Reverse(dataColl))
		t, err := time.Parse("2006-01-02", res.stockDataColl[0].traDay)
		if err != nil {
			return isNeedFilter, err
		}
		nowTime, _ := time.Parse("2006-01-02", pStock.req.end)
		if nowTime.Sub(t).Hours() > 48 {
			return isNeedFilter, fmt.Errorf("time interval is large than 48 hours")
		}
		if strings.Contains(names[res.code], "ST") {
			return isNeedFilter, fmt.Errorf("ignore ST stock")
		}
		isNeedFilter = false
	}
	return isNeedFilter, nil
}

func (pStock *Stock) reqContent(url string) ([]byte, error) {
	// nDialer := NewDialer("114.114.114.114:53")
	transCfg := &http.Transport{
		// DialContext:           nDialer.DialContext,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		MaxIdleConns:          500,
		IdleConnTimeout:       60 * time.Second,
		ExpectContinueTimeout: 30 * time.Second,
		MaxIdleConnsPerHost:   100,
		DisableKeepAlives:     true,
	}
	client := &http.Client{Transport: transCfg}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Println("can't create http new request")
		return nil, err
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("can't get response from url %s\n", url)
		return nil, err
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("can't read content from response body")
		return nil, err
	}

	return content, nil
}

func (pStock *Stock) FetchCode(code string) (*resp, error) {
	var content []byte
	var err error
	var res resp
	var url string

	pStock.req.end = time.Now().Format("2006-01-02")
	if pStock.req.intervalTraDay < MinFetchIntervalTraDay && pStock.req.intervalTraDay != 1 {
		url = fmt.Sprintf(historyReqAPI, code, pStock.req.end, MinFetchIntervalTraDay)
	} else {
		url = fmt.Sprintf(historyReqAPI, code, pStock.req.end, pStock.req.intervalTraDay)
	}
	content, err = pStock.reqContent(url)
	if err != nil {
		return nil, err
	}
	err = parseHistoryContent(&res, content, code)
	if err != nil {
		return nil, err
	}

	url = fmt.Sprintf(realTimeReqAPI, code)
	content, err = pStock.reqContent(url)
	if err != nil {
		return nil, err
	}

	err = parseRealTimeContent(&res, content)
	if err != nil {
		return nil, err
	}

	sort.Sort(sort.Reverse(res.stockDataColl))
	return &res, nil
}

func (pStock *Stock) fetchNames(codeColl []string) map[string]string {
	var l *limit = newLimit(100)
	var wg sync.WaitGroup
	var mu sync.Mutex
	var names map[string]string = make(map[string]string)
	var urlPrefix string = "https://qt.gtimg.cn/q=s_"
	for _, code := range codeColl {
		wg.Add(1)
		c := code
		f := func() {
			defer wg.Done()
			url := urlPrefix + c

			content, err := pStock.reqContent(url)
			if err != nil {
				fmt.Printf("reqContent err=%v\n", err)
				return
			}
			contentSplit := strings.Split(string(content), "~")
			if len(contentSplit) > 1 {
				name := contentSplit[1]
				mu.Lock()
				names[c] = name
				mu.Unlock()
			}
		}
		l.Run(f)
	}
	wg.Wait()

	return names
}

func (pStock *Stock) Fetch() {
	var ch chan *resp
	var l *limit = newLimit(25)
	var names map[string]string
	var codes []string

	if pStock.codeColl == nil {
		pStock.UpdateCodeColl()
		pStock.codeColl = pStock.ReadCodeColl()
	}

	ch = make(chan *resp, len(pStock.codeColl))

	names = pStock.fetchNames(pStock.codeColl)
	fmt.Println(names)
	pStock.respColl.resps = make([]resp, 0, len(pStock.codeColl))
	go func() {
		for res := range ch {
			var ok bool

			res := res
			if res.name, ok = names[res.code]; !ok {
				fmt.Printf("res.code %s not exist in names\n", res.code)
				continue
			}
			if isDuplicateElement(codes, res.code) {
				continue
			}
			pStock.respColl.mutex.Lock()
			pStock.respColl.resps = append(pStock.respColl.resps, *res)
			codes = append(codes, res.code)
			pStock.respColl.mutex.Unlock()
		}
	}()
	for i, code := range pStock.codeColl {
		pStock.wg.Add(1)
		f := func() {
			defer pStock.respColl.wg.Done()
			code := code
			for {
				res, err := pStock.FetchCode(code)
				if err != nil {
					fmt.Printf("fetchcode err=%v\n", err)
					continue
				}
				isNeedFilter, err := pStock.Filter(res, names)
				if err != nil {
					fmt.Printf("filter err=%v\n", err)
					break
				}
				if !isNeedFilter {
					ch <- res
				}
				break
			}
		}
		l.Run(f)
		count := i
		fmt.Println(count)
	}

	pStock.wg.Wait()

	close(ch)

}

func (pStock *Stock) GetCodes() codeColl {
	var codes codeColl
	for _, v := range pStock.respColl.resps {
		code := v.code
		codes = append(codes, code)
	}
	return codes
}

func (pStock *Stock) importDB(db *TradeDB) {
	var wg sync.WaitGroup
	var l *limit = newLimit(MaxConnections/2 - 1)
	var length int = len(pStock.respColl.resps)

	for i := 0; i < length; i++ {
		resp := pStock.respColl.resps[i]
		code := pStock.respColl.resps[i].code
		wg.Add(1)
		f := func() {
			defer wg.Done()
			db.NewTable(code)
			db.Create(resp)
		}
		l.Run(f)
	}
	wg.Wait()
}

func (pStock *Stock) String() string {
	var s string
	for n, v := range pStock.respColl.resps {
		if n == len(pStock.respColl.resps)-1 {
			s += v.code
		} else {
			s += v.code + ","
		}
	}
	return s
}

func (stockDataColl stockDataColl) Len() int {
	return len(stockDataColl)
}

func (stockDataColl stockDataColl) Less(i, j int) bool {
	timeI, err := time.Parse("2006-01-02", stockDataColl[i].traDay)
	if err != nil {
		log.Printf("time parse failed, err=%v\n", err)
	}
	timeJ, err := time.Parse("2006-01-02", stockDataColl[j].traDay)
	if err != nil {
		log.Printf("time parse failed, err=%v\n", err)
	}
	return timeI.Before(timeJ)
}

func (stockDataColl stockDataColl) Swap(i, j int) {
	stockDataColl[i], stockDataColl[j] = stockDataColl[j], stockDataColl[i]
}

package stock

import (
	"context"
	"net"
	"sync"
	"time"
)

type Dialer struct {
	dialer     *net.Dialer
	resolver   *net.Resolver
	nameserver string
}

var dnsMapCache sync.Map

// NewDialer create a Dialer with user's nameserver.
func NewDialer(nameserver string) *Dialer {
	dialer := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 60 * time.Second,
	}

	return &Dialer{
		dialer: dialer,
		resolver: &net.Resolver{
			Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
				return dialer.DialContext(ctx, "tcp", nameserver)
			},
		},
		nameserver: nameserver, // 用户设置的nameserver
	}
}

// DialContext connects to the address on the named network using
// the provided context.
func (d *Dialer) DialContext(ctx context.Context, network, address string) (net.Conn, error) {
	var res interface{}
	var ips []string

	host, port, err := net.SplitHostPort(address)
	if err != nil {
		return nil, err
	}

	res, ok := dnsMapCache.Load(address)

	if ok {
		ips = res.([]string)
	} else {
		ips, err = d.resolver.LookupHost(ctx, host) // 通过自定义nameserver查询域名
		if err != nil {
			return nil, err
		}
		dnsMapCache.Store(address, ips)
	}

	for _, ip := range ips {
		// 创建链接
		conn, err := d.dialer.DialContext(ctx, network, ip+":"+port)
		if err == nil {
			return conn, nil
		}
	}

	return d.dialer.DialContext(ctx, network, address)
}

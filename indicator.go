package stock

import (
	"fmt"
	"math"
	"time"
)

const CollUseLen = 360 * 3

type Action int

const (
	ActionBuy Action = iota + 0
	ActionSell
)

type indicators []indicator

type indicator struct {
	code           string
	date           string
	trade          Trade
	PETTM          float64
	PEDYNAMIC      float64
	ChangeHandRate float64
	day            indicatorDay
	month          indicatorMonth
}

type indicatorDay struct {
	WR1   float64
	WR2   float64
	RSI1  float64
	RSI2  float64
	RSI3  float64
	CCI   float64
	CCI2  float64
	KDJ   KDJ
	MACDs []MACD
	Boll  Boll
	EMAs
	MAs
	DMIs  []DMI
	Hands []float64
}

type indicatorMonth struct {
	WR1   float64
	WR2   float64
	RSI1  float64
	RSI2  float64
	RSI3  float64
	CCI   float64
	KDJ   KDJ
	MACDs []MACD
	Boll  Boll
	Hands []float64
}

type EMAs struct {
	EMA12  []float64
	EMA15  []float64
	EMA20  []float64
	EMA30  []float64
	EMA50  []float64
	EMA200 []float64
}

type MAs struct {
	MA1  []float64
	MA7  []float64
	MA14 []float64
	MA21 []float64
	MA28 []float64
}

type KDJ struct {
	K float64
	D float64
	J float64
}

type MACD struct {
	DIFF float64
	DEA  float64
	MACD float64
}

type Boll struct {
	Upper float64
	Mid   float64
	Lower float64
	Price float64
}

type DMI struct {
	PDI  float64
	MDI  float64
	ADX  float64
	ADXR float64
}

type indicatorInput struct {
	code     string
	duration int
	trades   Trades
	days     int // days to the lastest trading Day
	month    int
}

// W&R = 100 - (C - Ln)/(Hn - Ln) * 100
func (indIn *indicatorInput) genWR() float64 {
	var WR float64
	var max float64 = math.SmallestNonzeroFloat64
	var min float64 = math.MaxFloat64
	for i := indIn.days; i < indIn.duration; i++ {
		if indIn.trades[i].HighPrice > max {
			max = indIn.trades[i].HighPrice
		}
		if indIn.trades[i].LowPrice < min {
			min = indIn.trades[i].LowPrice
		}
	}
	WR = 100 - (indIn.trades[indIn.days].EndPrice-min)/(max-min)*100

	return WR
}

// CCI = ((TP - MA)/MD)/0.015
func (indIn *indicatorInput) genCCI() float64 {
	var CCI float64
	var TP float64
	var MASum float64
	var MDSum float64
	for i := indIn.days; i < indIn.duration+indIn.days; i++ {
		MASum += (indIn.trades[i].HighPrice + indIn.trades[i].LowPrice + indIn.trades[i].EndPrice) / 3
	}

	TP = (indIn.trades[indIn.days].HighPrice + indIn.trades[indIn.days].LowPrice + indIn.trades[indIn.days].EndPrice) / 3
	MA := MASum / float64(indIn.duration)
	for i := indIn.days; i < indIn.duration+indIn.days; i++ {
		MDSum += math.Abs((MA - (indIn.trades[i].HighPrice+indIn.trades[i].LowPrice+indIn.trades[i].EndPrice)/3))
	}
	MD := MDSum / float64(indIn.duration)

	CCI = ((TP - MA) / MD) / 0.015

	return CCI

}

// SMMA_t = a * diff + (1 - a)SMMA_y, a = 1/N
// RS = SMMA(U, n)/SMMA(D, n)
// RSI = (1 - 1/(1+RS))
func (indIn *indicatorInput) genRSI() float64 {
	var SMMAUp float64 = 0
	var SMMADown float64 = 0
	var a float64 = 1 / float64(indIn.duration)
	var RS float64
	var RSI float64
	var lastEndPrice float64
	for i := CollUseLen - 1; i >= indIn.days; i-- {
		if i == CollUseLen-1 {
			lastEndPrice = indIn.trades[i].EndPrice
			continue
		}
		diff := indIn.trades[i].EndPrice - lastEndPrice
		if diff > 0 {
			SMMADown = (1 - a) * SMMADown
			SMMAUp = a*diff + (1-a)*SMMAUp
		} else {
			SMMADown = a*math.Abs(diff) + (1-a)*SMMADown
			SMMAUp = (1 - a) * SMMAUp
		}
		lastEndPrice = indIn.trades[i].EndPrice
	}

	RS = SMMAUp / SMMADown
	RSI = 100 - (100 / (1 + RS))

	return RSI
}

// RSV = (Price - lowN)/(HighN - LowN) * 100
// Ki = 2/3 Ki-1 + 1/3 RSVi
// Di = 2/3 Di-1 + 1/3 Ki
func (indIn *indicatorInput) genKDJ() KDJ {
	var KDJ KDJ
	var K float64
	var D float64
	var max float64 = math.SmallestNonzeroFloat64
	var min float64 = math.MaxFloat64
	var rsv float64
	var a float64 = float64(1) / float64(3)
	for i := CollUseLen - 9; i >= indIn.days; i-- {
		max = indIn.trades.FindMax(i, i+9)
		min = indIn.trades.FindMin(i, i+9)
		if max == 0 || min == 0 {
			continue
		}
		rsv = ((indIn.trades[i].EndPrice - min) / (max - min)) * 100
		K = a*rsv + (1-a)*K
		D = a*K + (1-a)*D
	}
	KDJ.K = K
	KDJ.D = D
	KDJ.J = 3*K - 2*D
	return KDJ

}

// EMAi = EMAi-1*(1-2/(N+1)) + endPrice*(2/(N+1))
func (indIn *indicatorInput) genEMAs() EMAs {
	var emas EMAs
	fn := func(duration int) []float64 {
		var result []float64
		var EMA float64
		var a = 2 / (float64(duration) + 1)
		for i := CollUseLen - 1; i >= indIn.days; i-- {
			EMA = EMA*(1-a) + indIn.trades[i].EndPrice*(a)
			result = append([]float64{EMA}, result...)
		}
		return result
	}

	emas.EMA12 = fn(12)
	emas.EMA15 = fn(15)
	emas.EMA20 = fn(20)
	emas.EMA30 = fn(30)
	emas.EMA50 = fn(50)
	emas.EMA200 = fn(200)

	return emas
}

func (indIn *indicatorInput) genMAs() MAs {
	var mas MAs
	fn := func(duration int) []float64 {
		var result []float64
		var MASum float64
		var MA float64
		var count int
		for i := CollUseLen - 1; i >= 0; i-- {
			MASum += indIn.trades[i].EndPrice
			count++
			if count == duration {
				MA = MASum / float64(duration)
				result = append([]float64{MA}, result...)
				count--
				MASum -= indIn.trades[i+count].EndPrice
			}
		}
		return result
	}

	mas.MA1 = fn(1)
	mas.MA7 = fn(7)
	mas.MA14 = fn(14)
	mas.MA21 = fn(21)
	mas.MA28 = fn(28)

	return mas
}

func isInSameMonth(t1, t2 time.Time) bool {
	y1, m1, _ := t1.Date()
	y2, m2, _ := t2.Date()

	return y1 == y2 && m1 == m2
}

func (indIn *indicatorInput) newMonthTrades() Trades {
	var newTrades Trades
	var endTime time.Time

	if len(indIn.trades) == 0 {
		return Trades{}
	}

	endTime = indIn.trades[0].Date.AddDate(0, 0, -1*(indIn.days))

	for i, j := 0, 0; j < len(indIn.trades); j++ {
		k := j + 1
		if j < len(indIn.trades) && k < len(indIn.trades) && !isInSameMonth(indIn.trades[k].Date, endTime) {
			var t Trade

			t.HighPrice = indIn.trades.FindMax(i, j+1)
			t.LowPrice = indIn.trades.FindMin(i, j+1)
			t.StartPrice = indIn.trades[j].StartPrice
			t.EndPrice = indIn.trades[i].EndPrice
			newTrades = append(newTrades, t)

			i = k
			endTime = indIn.trades[k].Date
		}
	}

	return newTrades
}

//MACD MONTH
func (indIn *indicatorInput) genMonths() indicatorMonth {
	var macd MACD
	var DIFF float64
	var DEA float64
	var EMA12 float64
	var EMA26 float64
	var aEMA12 = 2 / (float64(12) + 1)
	var aEMA26 = 2 / (float64(26) + 1)
	var aDEA = 2 / (float64(9) + 1)
	var newTrades Trades
	var indMonth indicatorMonth

	newTrades = indIn.newMonthTrades()
	for i := len(newTrades) - 1; i >= indIn.days; i-- {
		EMA12 = EMA12*(1-aEMA12) + newTrades[i].EndPrice*(aEMA12)
		EMA26 = EMA26*(1-aEMA26) + newTrades[i].EndPrice*(aEMA26)
		DIFF = EMA12 - EMA26
		DEA = DEA*(1-aDEA) + DIFF*(aDEA)
		macd.DIFF = DIFF
		macd.DEA = DEA
		macd.MACD = 2 * (DIFF - DEA)
		indMonth.MACDs = append([]MACD{macd}, indMonth.MACDs...)
	}

	return indMonth
}

// MACD
func (indIn *indicatorInput) genDayMACDs() []MACD {
	var macd MACD
	var DIFF float64
	var DEA float64
	var EMA12 float64
	var EMA26 float64
	var aEMA12 = 2 / (float64(12) + 1)
	var aEMA26 = 2 / (float64(26) + 1)
	var aDEA = 2 / (float64(9) + 1)
	var macds []MACD

	for i := len(indIn.trades) - 1; i >= indIn.days; i-- {
		EMA12 = EMA12*(1-aEMA12) + indIn.trades[i].EndPrice*(aEMA12)
		EMA26 = EMA26*(1-aEMA26) + indIn.trades[i].EndPrice*(aEMA26)
		DIFF = EMA12 - EMA26
		DEA = DEA*(1-aDEA) + DIFF*(aDEA)
		macd.DIFF = DIFF
		macd.DEA = DEA
		macd.MACD = 2 * (DIFF - DEA)
		macds = append([]MACD{macd}, macds...)
	}

	return macds
}

func (indIn *indicatorInput) genBoll() Boll {
	var boll Boll
	var sum float64
	var average float64
	var subSquaresSum float64
	var SD float64

	for i := 0; i < indIn.duration; i++ {
		sum += indIn.trades[i].EndPrice
	}
	average = sum / float64(indIn.duration)

	for i := 0; i < indIn.duration; i++ {
		subSquaresSum += math.Pow(indIn.trades[i].EndPrice-average, 2)
	}

	SD = math.Sqrt(subSquaresSum / float64(indIn.duration))

	boll.Mid = average
	boll.Upper = average + 2*SD
	boll.Lower = average - 2*SD
	boll.Price = indIn.trades[0].EndPrice

	return boll

}

func (indIn *indicatorInput) genDMI() []DMI {
	var DMIs []DMI
	var count int
	var PDMSum float64
	var NDMSum float64
	var TRSum float64
	var PDM14 []float64
	var NDM14 []float64
	var TR14 []float64

	for i := len(indIn.trades) - 2; i >= indIn.days; i-- {
		var PDM float64
		var NDM float64
		var TR float64

		PDM = indIn.trades[i].HighPrice - indIn.trades[i+1].HighPrice
		if PDM <= 0 {
			PDM = 0
		}
		NDM = indIn.trades[i+1].LowPrice - indIn.trades[i].LowPrice
		if NDM <= 0 {
			NDM = 0
		}

		if PDM-NDM > 0 {
			NDM = 0
		} else if PDM-NDM < 0 {
			PDM = 0
		} else {
			PDM = 0
			NDM = 0
		}

		TR = math.Max(math.Max(math.Abs(indIn.trades[i].HighPrice-indIn.trades[i].LowPrice),
			math.Abs(indIn.trades[i].HighPrice-indIn.trades[i+1].EndPrice)),
			math.Abs(indIn.trades[i].LowPrice-indIn.trades[i+1].EndPrice))

		PDMSum += PDM
		NDMSum += NDM
		TRSum += TR
		if len(PDM14) == 14 {
			PDM14 = append(PDM14[1:], PDM)
			NDM14 = append(NDM14[1:], NDM)
			TR14 = append(TR14[1:], TR)
		} else {
			PDM14 = append(PDM14, PDM)
			NDM14 = append(NDM14, NDM)
			TR14 = append(TR14, TR)
		}
		count++
		if count == 14 {
			var PDI float64
			var MDI float64
			var dmi DMI

			PDI = (PDMSum / TRSum) * 100
			MDI = (NDMSum / TRSum) * 100
			dmi.PDI = PDI
			dmi.MDI = MDI
			DMIs = append([]DMI{dmi}, DMIs...)
			count--
			PDMSum -= PDM14[0]
			NDMSum -= NDM14[0]
			TRSum -= TR14[0]
		}
	}
	return DMIs
}

func (indIn *indicatorInput) genHands() []float64 {
	var hands []float64
	var hand float64
	for i := len(indIn.trades) - 1; i >= indIn.days; i-- {
		hand = indIn.trades[i].Hand
		hands = append([]float64{hand}, hands...)
	}
	return hands
}

func (ind *indicator) String() string {
	return fmt.Sprintf("code=%s, ind.WR1=%f, ind.WR2=%f, ind.RSI1=%f, ind.RSI2=%f, ind.RSI3=%f, ind.CCI=%f, ind.KDJ=%v, ind.MACD=%v\n",
		ind.code, ind.day.WR1, ind.day.WR2, ind.day.RSI1, ind.day.RSI2, ind.day.RSI3, ind.day.CCI, ind.day.KDJ, ind.day.MACDs)
}

func (kdj *KDJ) String() string {
	return fmt.Sprintf("{K:%f, D:%f, J:%f}", kdj.K, kdj.D, kdj.J)
}

func (macd *MACD) String() string {
	return fmt.Sprintf("{DIFF:%f, DEA:%f, MACD:%f}", macd.DIFF, macd.DEA, macd.MACD)
}

func (ind *indicator) isBuyTimeMonth() bool {
	var isPerfectMACD []int = []int{1, 2}
	var status int = 0

	for i, j := 0, 0; i < len(ind.month.MACDs) && j < len(isPerfectMACD); i++ {
		var change int
		temp := ind.month.MACDs[i]
		if i == 0 && len(ind.month.MACDs) > 0 {
			if temp.MACD < 0 && ind.month.MACDs[0].DIFF < -0.3 && ind.month.MACDs[0].DIFF > -0.5 &&
				ind.month.MACDs[0].MACD > ind.month.MACDs[1].MACD {
				status = 1
				continue
			} else {
				return false
			}
		}
		if temp.MACD < 0 {
			change = 1
		}
		if temp.MACD > 0 && temp.DIFF < 0 && temp.DEA < 0 {
			change = 2
		}

		if change == 0 {
			return false
		} else if status == 1 && change == 2 {
			status = 2
			j++
			if j == 1 {
				return true
			}
		}
	}

	return false
}

func (ind *indicator) isBuyTime() bool {
	var isPerfectEMA bool = false
	var isPerfectMA bool = false
	var isPerfectK bool = false
	var isPerfectDMI bool = false
	var isPerfectHand bool = false
	var isPerfectMACD bool = false
	var isPerfectPE bool = false
	var isPerfectChangeHandRate bool = false
	var isPerfectCCI = false
	var isPerfectPrice = false
	var isPerfectWR = false

	if ind.trade.EndPrice < 10 {
		isPerfectPrice = true
	}
	if ind.PETTM <= 30 && ind.PETTM > 0 &&
		ind.PEDYNAMIC <= 20 && ind.PEDYNAMIC > 0 {
		isPerfectPE = true
	} else {
		return false
	}
	// isPerfectPE = true

	if ind.ChangeHandRate > 2.5 {
		isPerfectChangeHandRate = true
	} else {
		return false
	}
	// isPerfectChangeHandRate = true

	// if len(ind.day.EMA12) > 1 && len(ind.day.EMA20) > 1 &&
	// 	len(ind.day.EMA30) > 0 && len(ind.day.EMA50) > 0 &&
	// 	ind.day.EMA12[0] <= ind.day.EMA20[0] &&
	// 	ind.day.EMA12[0] <= ind.day.EMA30[0] &&
	// 	ind.day.EMA12[0] <= ind.day.EMA50[0] {
	// ind.trade.EndPrice >= ind.day.EMA200[0] &&
	// ind.trade.EndPrice <= ind.day.EMA200[0]*1.05 {
	// isPerfectEMA = true
	// } else {
	// return false
	// }
	isPerfectEMA = true

	if len(ind.day.MA1) > 1 && len(ind.day.MA7) > 1 && len(ind.day.MA14) > 1 &&
		len(ind.day.MA21) > 1 && len(ind.day.MA28) > 1 {
		// ind.day.MA1[0] >= ind.day.MA28[0] {
		isPerfectMA = true
	} else {
		return false
	}
	isPerfectMA = true

	// if len(ind.day.Hands) > 1 &&
	// 	ind.day.Hands[0] > 200000 {
	// 	isPerfectHand = true
	// } else {
	// 	return false
	// }
	isPerfectHand = true
	if len(ind.day.MACDs) > 1 &&
		// ind.day.MACDs[0].MACD > ind.day.MACDs[1].MACD &&
		// math.Abs(ind.day.MACDs[0].MACD) < 0.1 &&
		// ind.day.MACDs[1].DIFF <= 0 {
		ind.day.MACDs[0].DIFF < 0 && ind.day.MACDs[0].MACD > 0 {
		// ind.month.MACDs[0].MACD > 0 {
		isPerfectMACD = true
	} else {
		return false
	}
	// isPerfectMACD = true
	// // high 5.97 start 5.80  low 5.69 end 5.78
	// high 11.80 start 11.28 low 11.09 end 11.38
	// if (ind.trade.HighPrice-ind.trade.EndPrice)/ind.trade.StartPrice >= 0.03 &&
	// 	math.Abs(ind.trade.LowPrice-ind.trade.EndPrice)/ind.trade.StartPrice <= 0.03 &&
	// 	math.Abs(ind.trade.StartPrice-ind.trade.EndPrice)/ind.trade.StartPrice <= 0.01 {
	// 	isPerfectK = true
	// }
	isPerfectK = true

	// if len(ind.day.DMIs) > 1 &&
	// 	ind.day.DMIs[0].MDI > ind.day.DMIs[0].PDI {
	// 	// ind.day.DMIs[0].MDI < ind.day.DMIs[1].MDI &&
	// 	// ind.day.DMIs[0].MDI-ind.day.DMIs[0].PDI <= 15 {
	// 	isPerfectDMI = true
	// } else {
	// 	return false
	// }
	isPerfectDMI = true

	// if ind.day.CCI2 < -30 && ind.day.CCI > -10 {
	// if ind.day.CCI > -30 && ind.day.CCI < 100 {
	// 	isPerfectCCI = true
	// }
	// if ind.day.CCI >= 100 {
	// 	isPerfectCCI = true
	// }
	isPerfectCCI = true

	// if ind.day.WR1 >= 80 {
	// 	isPerfectWR = true
	// }
	isPerfectWR = true
	if isPerfectEMA && isPerfectMA && isPerfectK &&
		isPerfectDMI && isPerfectHand && isPerfectMACD &&
		isPerfectPE && isPerfectChangeHandRate && isPerfectCCI &&
		isPerfectPrice && isPerfectWR {
		return true
	}

	return false
}

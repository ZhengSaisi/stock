package stock

import (
	"fmt"
	"log"
	"math"
	"sort"
	"sync"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const MaxConnections = 100

type TradeDB gorm.DB

type Trade struct {
	gorm.Model
	Code           string    `gorm:"not null;comment:'股票代码'"`
	Date           time.Time `gorm:"not null;comment:'日期';unique"`
	HighPrice      float64   `gorm:"not null;comment:'最高值'"`
	LowPrice       float64   `gorm:"not null;comment:'最低值'"`
	StartPrice     float64   `gorm:"not null;comment:'开盘价'"`
	EndPrice       float64   `gorm:"not null;comment:'收盘价'"`
	Hand           float64   `gorm:"not null;comment:'总手'"`
	ChangeHandRate float64   `gorm:"not null;comment:'换手率'"`
	PETTM          float64   `gorm:"not null;comment:'市盈率TTM'"`
	PEDYNAMIC      float64   `gorm:"not null;comment:'市盈率动态'"`
}

type Trades []Trade

// var mutexMap map[string]sync.RWMutex
var mutexMap sync.Map

func NewDB() (*gorm.DB, error) {
	host := "127.0.0.1"
	port := "3306"
	user := "stock"
	pass := "123456"
	name := "stock"
	dsn := user + ":" + pass + "@tcp(" + host + ":" + port + ")/" + name + "?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println("Can't connect to database")
		return nil, err
	}
	sqlDB, err := db.DB()
	if err != nil {
		return nil, err
	}
	sqlDB.SetMaxOpenConns(MaxConnections)
	return db, nil
}

func (t *TradeDB) NewTable(code string) {
	var mutexRW sync.RWMutex
	var tableName string = "Trade_" + code

	db := (*gorm.DB)(t)

	mutexMap.LoadOrStore(tableName, &mutexRW)

	db = db.Table(tableName)

	db.AutoMigrate(&Trade{Code: code})
}

func (t *TradeDB) Create(resp resp) {
	var coll stockDataColl = resp.stockDataColl
	var trades Trades = make(Trades, 0, len(coll))
	var tableName string

	if len(coll) == 0 {
		return
	}
	tableName = "Trade_" + coll[0].code
	db := (*gorm.DB)(t)
	for i := 0; i < len(coll); i++ {
		var trade Trade

		trade.Code = coll[i].code
		trade.Date, _ = time.Parse("2006-01-02", coll[i].traDay)
		trade.HighPrice = coll[i].highPrice
		trade.LowPrice = coll[i].lowPrice
		trade.StartPrice = coll[i].startPrice
		trade.EndPrice = coll[i].endPrice
		trade.Hand = coll[i].totalHand
		trade.PETTM = resp.PETTM
		trade.PEDYNAMIC = resp.PEDYNAMIC
		trade.ChangeHandRate = resp.ChangeHandRate

		trades = append(trades, trade)
	}
	db = db.Table("Trade_" + coll[0].code)

	m, ok := mutexMap.Load(tableName)
	if !ok {
		return
	}

	mu, ok := m.(*sync.RWMutex)
	if ok {
		mu.Lock()
		defer mu.Unlock()
	}
	db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "date"}},
		DoUpdates: clause.AssignmentColumns([]string{"high_price", "low_price", "start_price", "end_price", "hand", "pettm", "pedynamic", "change_hand_rate"}),
	}).Create(&trades)
}

func (t *TradeDB) Find(code string) []Trade {
	var trades Trades
	var tableName = "Trade_" + code

	db := (*gorm.DB)(t)
	db = db.Table(tableName)

	m, ok := mutexMap.Load(tableName)
	if !ok {
		fmt.Println("can't get mutex from mutexMap")
		return []Trade{}
	}

	mu, ok := m.(*sync.RWMutex)
	if ok {
		mu.Lock()
		defer mu.Unlock()
	}

	db.Find(&trades)

	sort.Sort(sort.Reverse(trades))
	return trades
}

func (t *TradeDB) AddOrUpdate(s *stockData) {
	var tableName string
	var trade Trade

	if s == nil {
		return
	}

	tableName = "Trade_" + s.code
	db := (*gorm.DB)(t)
	db = db.Table(tableName)

	m, ok := mutexMap.Load(tableName)
	if !ok {
		fmt.Println("can't get mutex from mutexMap")
		return
	}

	mu, ok := m.(*sync.RWMutex)
	if ok {
		mu.Lock()
		defer mu.Unlock()
	}
	trade.Code = s.code
	trade.Date, _ = time.Parse("2006-01-02", s.traDay)
	trade.HighPrice = s.highPrice
	trade.LowPrice = s.lowPrice
	trade.StartPrice = s.startPrice
	trade.EndPrice = s.endPrice
	trade.Hand = s.totalHand
	db.Clauses(clause.OnConflict{
		Columns:   []clause.Column{{Name: "date"}},
		DoUpdates: clause.AssignmentColumns([]string{"high_price", "low_price", "start_price", "end_price", "hand", "pettm", "pedynamic", "change_hand_rate"}),
	}).Create(&trade)

}

func (ts Trades) Len() int {
	return len(ts)
}

func (ts Trades) Less(i, j int) bool {
	return ts[i].Date.Before(ts[j].Date)
}

func (ts Trades) Swap(i, j int) {
	ts[i], ts[j] = ts[j], ts[i]
}

func (ts Trades) FindMax(start int, end int) float64 {
	var max float64 = math.SmallestNonzeroFloat64
	sort.Sort(sort.Reverse(ts))
	if ts.Len() < end {
		log.Printf("interval bigger then length of stock Data Collection")
		return 0
	}
	for i := start; i < end; i++ {
		if ts[i].HighPrice > max {
			max = ts[i].HighPrice
		}
	}
	return max
}

func (ts Trades) FindMin(start int, end int) float64 {
	var min float64 = math.MaxFloat64
	sort.Sort(sort.Reverse(ts))
	if ts.Len() < end {
		log.Printf("interval bigger then length of stock Data Collection")
		return 0
	}
	for i := start; i < end; i++ {
		if ts[i].LowPrice < min {
			min = ts[i].LowPrice
		}
	}
	return min
}

func (t *TradeDB) genIndicator(code string) (*indicator, error) {
	var indIn indicatorInput
	var ind indicator
	var err error
	var trades Trades

	if err != nil {
		log.Printf("select data failed, err=%v\n", err)
		return nil, err
	}
	if CollUseLen > MinFetchIntervalTraDay {
		return nil, fmt.Errorf("MinFetchIntervalTraDay less then CollUseLen")
	}
	trades = t.Find(code)
	if len(trades) < CollUseLen {
		return nil, fmt.Errorf("Table=Trade_%s, length of stockDataColl less then CollUseLen", code)
	}

	ind.code = code
	ind.date = trades[0].Date.AddDate(0, 0, 0).Format("2006-01-02")
	ind.trade = trades[0]
	ind.PETTM = trades[0].PETTM
	ind.PEDYNAMIC = trades[0].PEDYNAMIC
	ind.ChangeHandRate = trades[0].ChangeHandRate

	indIn.code = code
	indIn.trades = trades

	indIn.duration = 10
	ind.day.WR1 = indIn.genWR()

	indIn.duration = 6
	ind.day.WR2 = indIn.genWR()

	indIn.duration = 6
	ind.day.RSI1 = indIn.genRSI()

	indIn.duration = 12
	ind.day.RSI2 = indIn.genRSI()

	indIn.duration = 24
	ind.day.RSI3 = indIn.genRSI()

	indIn.duration = 14
	ind.day.CCI = indIn.genCCI()

	indIn.days = 1
	ind.day.CCI2 = indIn.genCCI()

	indIn.duration = 9
	ind.day.KDJ = indIn.genKDJ()

	ind.day.MACDs = indIn.genDayMACDs()
	ind.month = indIn.genMonths()

	indIn.duration = 20
	ind.day.Boll = indIn.genBoll()

	ind.day.EMAs = indIn.genEMAs()

	ind.day.MAs = indIn.genMAs()

	ind.day.DMIs = indIn.genDMI()

	ind.day.Hands = indIn.genHands()

	return &ind, nil
}

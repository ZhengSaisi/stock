package stock

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

type filteredStock struct {
	code string
}

type filteredStocks []filteredStock

const (
	NormalPrices = iota
	MediumPrices
	LowPrices_1_4
	LowPrices_3_8
	LowPrices_5_16
)

func (filters filteredStocks) String() {
	for k, v := range filters {
		fmt.Printf("%d: %s\n", k, v.code)
	}
}

func (filters filteredStocks) GetCodes() codeColl {
	var codes codeColl
	for _, f := range filters {
		code := f.code
		codes = append(codes, code)
	}
	return codes
}
